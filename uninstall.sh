#!/bin/bash

# Make sure only root can run our script
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

# get the logged in username and the path to the CCC directory
user="$(logname)"

#get user's home directory
if [ $user = "root" ]; then
	home="/root"
else
	home="$(cat /etc/passwd | grep $user | cut -d':' -f 6)"
fi

if rm /etc/cron.d/CCCPull &&
   rm /usr/bin/CCCSync &&
   rm /usr/share/icons/OSCicon.png &&
   rm -rf /usr/share/CCCSync &&
   rm -rf $home/.CCCSync &&
   rm /usr/share/applications/CCCSync.desktop; then
	echo "Uninstalled successfully!"
else
	rm /etc/cron.d/CCCPull
	rm /usr/bin/CCCSync
	rm /usr/share/icons/OSCicon.png
	rm -rf /usr/share/CCCSync
	rm -rf $home/.CCCSync
	rm /usr/share/applications/CCCSync.desktop
	echo "Something went wrong; most probably some of the installed files were missing. I removed what I could find."
fi
