#!/bin/bash
# gets the CCC directory from the user

cccdir="$(zenity --file-selection --directory --title 'Choose the CCC directory' 2>/dev/null)"

case $? in
	0)
		echo "$cccdir";;
	1)
		return 1;;
	-1)
		return 1;;
esac
