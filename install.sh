#!/bin/bash

# Make sure only root can run our script
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

# get the logged in username and the path to the CCC directory
user="$(logname)"

#get user's home directory
if [ $user = "root" ]; then
	home="/root"
else
	home="$(cat /etc/passwd | grep $user | cut -d':' -f 6)"
fi

# get the CCC directory
cccdir="$(su $user zenity.sh)"
if [[ $? -eq 1 ]]; then
	# cancel button pressed
	exit
fi

# go to CCC directory, make sure the git repo is initialized and that the remotes are set correctly and then come back to the current directory to continue the script
current_dir="$(pwd)"
cd $cccdir
su $user <<'EOF'
git init
fatal="$(git remote add origin git@github.com:Open-Source-Community/CCC.git 2>&1 > /dev/null)"
if [ "$fatal" = "fatal: remote origin already exists." ]; then
	git remote set-url origin git@github.com:Open-Source-Community/CCC.git
fi
EOF
cd $current_dir

# generate a desktop file that is hardwired to the CCC directory
cat << EOF > CCCSync.desktop
[Desktop Entry]
Type=Application
Encoding=UTF-8
Name=CCC Sync
Comment=This is a little script that syncronizes the CCC repository
Exec=CCCSync $cccdir
Icon=/usr/share/icons/OSCicon.png
Terminal=true
EOF

# give the desktop file the execute permession that it needs to run.
chmod +x CCCSync.desktop

# generate the git_pull.sh script, binded to the home directory of the user
cat << EOF > git_pull.sh
pull_lock="\$(cat $home/.CCCSync/pull_lock)"

if [ "\$pull_lock" = "0" ]; then
	echo "1" > $home/.CCCSync/pull_lock && git pull
	echo "0" > $home/.CCCSync/pull_lock
fi
EOF

chmod +x git_pull.sh

# cron job that pulls every 5 mins, and puts everything in place, removes everything in case anything failed to be copied/created successfully.
if echo "*/5 * * * * $user cd $cccdir && bash /usr/share/CCCSync/git_pull.sh" > /etc/cron.d/CCCPull &&
   cp CCCSync /usr/bin/ &&
   cp OSCicon.png /usr/share/icons/ &&
   cp CCCSync.desktop /usr/share/applications/ &&
   mkdir /usr/share/CCCSync &&
   cp git_pull.sh /usr/share/CCCSync/ &&
   su $user -c "mkdir $home/.CCCSync" &&
   su $user -c "echo '0' > $home/.CCCSync/pull_lock" &&
   su $user configure_ssh.sh; # Check for SSH keys and if they're not their create them.
    then
	echo "Installed successfully!"
	exit 0
else
	rm /usr/bin/CCCSync
	rm /usr/share/icons/OSCicon.png
	rm /usr/share/applications/CCCSync.desktop
	rm -rf /usr/share/CCCSync
	rm -rf $home/.CCCSync
	echo "Something went wrong... I deleted the files I installed."
fi
