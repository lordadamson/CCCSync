# create the .ssh directory if it doesn't exist
if [ ! -d ~/.ssh ]; then
	mkdir -p ~/.ssh/
fi

# generate a public key in case it doesn't exist
if [ ! -f ~/.ssh/id_rsa.pub ]; then
       	ssh-keygen -t rsa -N "" -f ~/.ssh/id_rsa
       	echo "Execute ssh-keygen --[done]"
fi

# register the public key
eval `ssh-agent -s` >/dev/null
ssh-add ~/.ssh/id_rsa 2>/dev/null

# display public key with zenity
file=~/.ssh/id_rsa.pub
zenity --text-info \
       --title="ssh public key" \
       --filename=$file \
       --checkbox="Copy the text above and go to your github settings and add a new ssh key and paste it." 2>/dev/null

case $? in
    0)
	# test access to github
	ssh -To 'StrictHostKeyChecking no' git@github.com 2> /tmp/sshout
	sshout="$(cat /tmp/sshout | grep successfully)"
	;;
    1)
        exit 1
	;;
    -1)
        exit 1
	;;
esac

echo "$sshout"
if [ -n "$sshout" ]; then
	exit 0
else
	exit 1
fi
